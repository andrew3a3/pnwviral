require('./bootstrap');

window.Vue = require('vue');
import Vue from 'vue'
import Vuetify from 'vuetify'
Vue.use(Vuetify)

// //Main Components
Vue.component('app-home', require('./components/AppHome.vue').default);

import router from './Router/router.js';
const app = new Vue({
    el: '#app',
    router,
    vuetify: new Vuetify()
});
