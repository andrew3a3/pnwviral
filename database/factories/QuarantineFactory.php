<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Quarantine;
use Faker\Generator as Faker;

$factory->define(Quarantine::class, function (Faker $faker) {
    return [
        'region_id'=> 1,
        'case_id' => $faker->word,
        'location'=> $faker->text($maxNbChars = 50) ,
        'latitude'=> $faker->latitude($min = -90, $max = 90) ,
        'longitude'=> $faker->longitude($min = -180, $max = 180)
    ];
});
