<?php

use Illuminate\Database\Seeder;
use App\ConfirmedCase;

class CaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(ConfirmedCase::class, 25000)->create()->each(function($u){
            //$u->posts()->save(factory(ConfirmedCase::class)->make());
        });
    }
}
