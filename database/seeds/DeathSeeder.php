<?php

use Illuminate\Database\Seeder;
use App\Death;

class DeathSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Death::class, 25000)->create()->each(function($u){
            //$u->posts()->save(factory(Death::class)->make());
        });
    }
}
