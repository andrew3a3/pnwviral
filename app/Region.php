<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Region extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'region', 'population', 'long_name'
    ];


    public function reports() {
        return $this->hasMany('App\WHOReport');
    }

    public function deaths() {
        return $this->hasMany('App\Death');
    }

    public function confirmedCases() {
        return $this->hasMany('App\ConfirmedCase');
    }

    public function quarantines() {
        return $this->hasMany('App\Quarantine');
    }
}
