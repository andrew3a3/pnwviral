<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;
    public $incrementing = false;
    public $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'admin'=>'boolean',
    ];

    public function wellnessChecks() {
        return $this->hasMany('App\WellnessCheck');
    }

    public function checkins() {
        return $this->hasMany('App\CheckIns');
    }


    public static function getRandomId() {
        $id = NULL;

        while($id == NULL) {
            $temp = strtolower(str_random(20));

            if(!User::where('id', $temp).first()) {
                $id = $temp;
            }
        }

        return $id;
    }
}
