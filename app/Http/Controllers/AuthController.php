<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Socialite;
use Hash;
use Str;
use App\User;

class AuthController extends Controller
{
    public function google() {
        return Socialite::driver('google')->redirect();
    }

    public function googleRedirect() {
        $user = Socialite::driver('google')->user();
        //dd($user);
        $user = User::firstOrCreate([
            'email'=> $user->email],
            ['id'=> User::getRandomId(),
            'name'=> $user->name,
            'password'=> Hash::make(Str::random(24))
            ]
        );

        Auth::login($user, true);

        return redirect('/');
    }

    public function facebook() {
        return Socialite::driver('facebook')->redirect();
    }

    public function facebookRedirect() {
        $user = Socialite::driver('facebook')->user();
        //dd($user);
        $user = User::firstOrCreate([
            'email'=> $user->email],
            ['id'=> User::getRandomId(),
            'name'=> $user->name,
            'password'=> Hash::make(Str::random(24))
            ]
        );

        Auth::login($user, true);

        return redirect('/');
    }
}
