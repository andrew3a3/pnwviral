<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Libs\Haversine;
use App\Region;
use App\Death;
use App\ConfirmedCase;
use App\Quarantine;
use App\WHOReport;
use App\WellnessCheck;
use App\CheckIn;
use Illuminate\Support\Facades\Auth;

class SiteController extends Controller
{
    private $latitude;
    private $longitude;

    public function fetchData(Request $request) {
        $region = $request->has('region')? $request->region: NULL;
        $latitude = $request->has('latitude')? $request->latitude: NULL;
        $longitude = $request->has('longitude')? $request->longitude: NULL;

        //fetch data
        $deaths = Death::orderBy('created_at', 'desc')->get();
        $cases = ConfirmedCase::orderBy('created_at', 'desc')->get();
        $quarantines = Quarantine::orderBy('created_at', 'desc')->get();

        //Sort if possible
        if($latitude && $longitude) {
            $this->prioritizeDeaths($latitude, $longitude);
             $this->prioritizeCases($latitude, $longitude);
            $this->prioritizeQuarantines($latitude, $longitude);
        }


        $reports = $this->getLatestReports();
        $regions = Region::orderBy('created_at', 'desc')->get();

        return array(
            "reports"=> $reports,
            "regions"=>$regions,
            "deaths"=>$deaths,
            "cases"=>$cases,
            "quarantines"=>$quarantines);
    }

    private function prioritizeDeaths($deaths) {
        $this->quickSort($deaths, 0, count($deaths) - 1);
    }

    private function prioritizeCases($cases) {
        $this->quickSort($cases, 0, count($cases) -1);
    }

    private function prioritizeQuarantines($quarantines) {
        $this->quickSort($quarantines, 0, count($quarantines) - 1);
    }

    // SORTING ------------------------------------------------------------------------------------------------------------------
    private function partition($arr, $start, $end)
    {
        $pivot = $arr[$end];
        $index = $start - 1;

        for($i = $start; $i <= $end - 1;  $i++) {

            //Fill Attribute
            if($pivot->distance == Null){
                $coords1 = [$pivot->latitude == NULL? 0: $pivot->latitude,
                    $pivot->longitude == NULL? 0 : $pivot->longitude];
                $dist1 = Haversine::getDistance($coords1[0], $coords1[1],
                $this->latitude, $this->longitude);

                $pivot->distance = $dist1;
            }

            $temp = $arr[$i];
            //Fill Attribute
            if($temp->distance == NULL) {
                $coords2= [$temp->latitude == NULL? 0: $temp->latitude,
                    $temp->longitude == NULL? 0 : $temp->longitude];

                $dist2 = Haversine::getDistance($coords2[0], $coords2[1],
                    $this->latitude, $this->longitude);

                $temp->distance = $dist2;
            }

            //Perform Comparison
            if($temp->distance < $pivot->distance) {
                $index++;
                //swap
                $curr1 = $arr[$index];
                $curr2 = $arr[$i];
                $arr[$index] = $curr2;
                $arr[$i] = $curr1;
            }
        }

        //put pivot in correct slot
        $next = $arr[$index + 1];
        $arr[$end] = $next;
        $arr[$index + 1] = $pivot; //pivot is placed into correct slot of array

        return $index + 1;
    }


    private function quickSort($arr, $start, $end)
    {
        if($start < $end) {
            $partition = $this->partition($arr, $start, $end);
            $this->quickSort($arr, $start, $partition - 1); #Sort left half
            $this->quickSort($arr, $partition + 1, $end); # Sort Right Half
        }
    }
    //------------------------------------------------------------------------------------------------------------------------

    //Gets the most upto data report for each region
    private function getLatestReports() {
        $arr = array();

        $regions = Region::all();

        foreach($regions as $region) {
            $report = WHOReport::orderBy('created_at', 'desc')
            ->where('region_id', $region->id)
            ->first();

            array_push($arr, $report);
        }

        return $arr;
    }

    public function createWellnessCheck(Request $request) {
        if(Auth::check() == false)
            return false;

        $data = $request->all();
        $wellness = WellnessCheck::create(array_merge($data,
            ['user_id'=> Auth::user()->id]));

        return $wellness->isValid();
    }

    public function createCheckin(Request $request) {
        if(Auth::check() == false)
            return false;

        $data = $request->all();
        $checkin = CheckIn::create(array_merge($data,
            ['user_id'=> Auth::user()->id]));

        return $checkin->isValid();
    }
}
