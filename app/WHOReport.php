<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WHOReport extends Model
{
    use SoftDeletes;

    protected $fillable = [
        '24_hr_cases', '24_hr_suspect_cases',
        '24_hr_deaths', 'cumulative_cases',
        'cumulative_deaths', 'region_id'
    ];

    public function region() {
        return $this->belongsTo('App\Region');
    }
}
