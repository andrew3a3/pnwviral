<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;

class CheckIn extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'user_id', 'comment',
        'latitude', 'longitude'
    ];

    public function user() {
        return $this->belongsTo('App\User');
    }
}
