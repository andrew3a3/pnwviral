<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;


use Illuminate\Database\Eloquent\Model;

class ConfirmedCase extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'region_id', 'case_id',
        'location', 'latitude',
        'longitude'
    ];

    protected $appends = array('distance');

    public function getDistanceAttribute()
    {
        return $this->distance;
    }

    public function setDistanceAttribute($value) {
        $this->attributes['distance'] = $value;
    }

    public function region() {
        return $this->belongsTo('App\Region');
    }
}
