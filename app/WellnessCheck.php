<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;

class WellnessCheck extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'user_id', 'wellness_score', 'comment',
        'remind_me', 'remind_in'
    ];

    protected $casts = [
            'remind_me'=>'boolean',
    ];

    public function user() {
        return $this->belongsTo('App\User');
    }
}
