let hide = true;

$('#stats-btn').click(function() {
    hide = !hide;

    if(hide) {
        $('#stats-panel').addClass('stats--hide')
        $('#stats-direction').css("transform", "rotateZ(0deg)");
    }else{
        $('#stats-panel').removeClass('stats--hide')
        $('#stats-direction').css("transform", "rotateZ(180deg)");
    }
})
