let currentPosition = null;
let myState = null;


$('#map').css("visibility", "hidden");
$('#legend').hide();

if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(showPosition);
}

function showPosition(position) {
    currentPosition = position;
    console.log(currentPosition);
}

mapboxgl.accessToken =
    'pk.eyJ1IjoiYW5kcmV3M2EzIiwiYSI6ImNqZzRvdHJzbzBvMTgyd3FkNHloc2dzZHgifQ.zgsODNBS7nUBLdLq_oaW7A';
let map = new mapboxgl.Map({
    style: 'mapbox://styles/mapbox/light-v10',
    center: [-122.342705, 47.615701],
    zoom: 15.25,
    pitch: 45,
    bearing: -17.6,
    container: 'map',
    antialias: true
});

// The 'building' layer in the mapbox-streets vector source contains building-height
// data from OpenStreetMap.
map.on('load', function () {
    $('#loader').hide();
    $('#map').css("visibility", "visible");
    $('#legend').show();
    // Insert the layer beneath any symbol layer.
    var layers = map.getStyle().layers;

    var labelLayerId;
    for (var i = 0; i < layers.length; i++) {
        if (layers[i].type === 'symbol' && layers[i].layout['text-field']) {
            labelLayerId = layers[i].id;
            break;
        }
    }

    map.addLayer({
            'id': '3d-buildings',
            'source': 'composite',
            'source-layer': 'building',
            'filter': ['==', 'extrude', 'true'],
            'type': 'fill-extrusion',
            'minzoom': 15,
            'paint': {
                'fill-extrusion-color': '#aaa',

                // use an 'interpolate' expression to add a smooth transition effect to the
                // buildings as the user zooms in
                'fill-extrusion-height': [
                    'interpolate',
                    ['linear'],
                    ['zoom'],
                    15,
                    0,
                    15.05,
                    ['get', 'height']
                ],
                'fill-extrusion-base': [
                    'interpolate',
                    ['linear'],
                    ['zoom'],
                    15,
                    0,
                    15.05,
                    ['get', 'min_height']
                ],
                'fill-extrusion-opacity': 0.6
            }
        },
        labelLayerId
    );

    if (currentPosition != null) {
        coords = [currentPosition.coords.longitude,
            currentPosition.coords.latitude
        ];
        // map.flyTo({
        //     center: coords
        // });

        flyTo(currentPosition.coords.latitude,
            currentPosition.coords.longitude);
    }
});


function flyTo(latitude, longitude) {
    map.flyTo({
        center: [longitude, latitude]
    })
}
