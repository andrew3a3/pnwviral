    //Sidenav
    $(document).ready(function () {
        $('.sidenav').sidenav();
    });

    //Tooltips
    $(document).ready(function () {
        $('.tooltipped').tooltip();
    });

    //Floating Action Button
    $(document).ready(function () {
        $('.fixed-action-btn').floatingActionButton();
    });

    //Modal
    $(document).ready(function () {
        $('.modal').modal();
    });
