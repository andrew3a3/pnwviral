<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/fetch-data', 'SiteController@fetchData');


//AUTH
Route::prefix('sign-in')->group(function(){
    //Google Signin
Route::get('/google', 'AuthController@google')->name('auth.google');
Route::get('/google/redirect', 'AuthController@googleRedirect')->name('auth.google-redirect');

    //Facebook Signin
Route::get('/facebook', 'AuthController@facebook')->name('auth.facebook');
Route::get('/facebook/redirect', 'AuthController@facebookRedirect')->name('auth.facebook-redirect');
});

//ACTIVITY
Route::prefix('activity')->group(function(){
    Route::post('/create-wellness', 'SiteController@createWellnessCheck')->name('activity.createWellnessCheck');
    Route::post('/create-checkin', 'SiteController@createCheckin')->name('activity.createCheckin');
});
